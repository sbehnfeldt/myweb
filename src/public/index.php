<?php
namespace behnfeldt;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

require_once '../vendor/autoload.php';


$app = new \Slim\App([
    'notFoundHandler' => function($c) {
        return function($request, $response) use ($c) {
            $loader = new \Twig\Loader\FilesystemLoader('../templates');
            $twig = new \Twig\Environment($loader, [
                'cache' => false
            ]);
            return $response->withStatus(404)
                            ->withHeader('Content-Type', 'text/html')
                            ->write( $twig->render( '404.html.twig', [
                                'title' => 'Page Not Found'
                            ]));
        };
    }
]);

$app->get( '/', function( Request $request, Response $response ) {
    $loader = new FilesystemLoader('../templates');
    $twig = new Environment($loader, [
        'cache' => false
    ]);
    echo $twig->render( 'index.html.twig', [
        'name' => 'Index'
    ]);
});


$app->get( '/blog', function( Request $request, Response $response ) {
    $loader = new FilesystemLoader('../templates');
    $twig = new Environment($loader, [
        'cache' => false
    ]);
    echo $twig->render( 'blog.html.twig', [
        'name' => 'Blog'
    ]);
});

$app->run();
